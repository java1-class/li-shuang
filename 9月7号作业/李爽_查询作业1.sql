create table student2
(
	student_id int(4) primary key not null auto_increment,
	name varchar(10),
	sex varchar(2),
	birthday date,
	profession int(1)
);

create table course
(
	id int(4) primary key not null auto_increment,
	student_id int(4),
	semester int(2),
	class_on int(2),
	score double(6,1),
	FOREIGN KEY (student_id) REFERENCES student2(student_id)
);

select name,sum(score) as '优' from student2 s,course c where c.score > 90 and s.student_id = c.student_id group by name

select name,avg(score) as '良' from student2 s,course c where c.score > 80 and c.score < 90 and s.student_id = c.student_id group by name