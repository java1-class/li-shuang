create table student3
(
	s_id int(4) primary key not null auto_increment,
	name varchar(10),
	Tel varchar(11),
	Content varchar(5),
	Date date,
	t_id int(4),
	FOREIGN KEY (t_id) REFERENCES teacher2(t_id)
);

create table teacher2
(
	t_id int(4) primary key not null auto_increment,
	name varchar(10),
	age int(5),
	address varchar(30)
);

insert into student3 (name,Tel,Content,Date,t_id) values('小王','13254748547','高中毕业','2007-05-06',1);

select t.name,count(s.s_id) from student3 s,teacher2 t where s.t_id = t.t_id group by t.name

select t.name,count(s.s_id) from student3 s,teacher2 t where s.Content = '大专毕业' and s.t_id = t.t_id group by t.name