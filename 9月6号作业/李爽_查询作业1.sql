create table product
(
	p_id int(4) primary key not null auto_increment,
	p_Name varchar(10),
	price double,
	num int(10),
	company varchar(10),
	address varchar(5)
);

select * from product;

insert into product (p_Name,price,num,company,address) values('电视',1000.00,800,'长虹','成都');
insert into product (p_Name,price,num,company,address) values('电视',1000.00,1000,'海尔','成都');
insert into product (p_Name,price,num,company,address) values('冰箱',1800.00,300,'长虹','北京');
insert into product (p_Name,price,num,company,address) values('冰箱',3500.00,500,'海尔','重庆');
insert into product (p_Name,price,num,company,address) values('服装',280.00,1000,'雅戈尔','成都');
insert into product (p_Name,price,num,company,address) values('服装',1200.00,200,'雅戈尔','北京');
insert into product (p_Name,price,num,company,address) values('手机',2500.00,200,'华为','深圳');
insert into product (p_Name,price,num) values('手机',200.00,1200);
insert into product (p_Name,price,num) values('手机',204.00,1500);
insert into product (p_Name,price,num) values('手机',202.00,1000);

update product set num = 1000 where p_Name = '电视';
update product set price = 2500 where p_Name = '手机';

delete from product where (price between 1000 and 2000) and p_Name = '手机'

select * from product where (price > 200 and p_Name = '服装') or (price <5000 and p_Name = '冰箱');