create table Student
(
	s_id int(4) primary key not null auto_increment,
	s_number int(10),
	s_name varchar(10),
	s_age int(3),
	s_tel varchar(11),
	s_address varchar(50),
	s_score double
);

select * from Student

insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(111111,'李爽',20,'18982379506','四川',100.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(222222,'张三',18,'18982379506','四川',44.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(333333,'李四',17,'18982379506','四川',99.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(444444,'王五',22,'18982379506','四川',78.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(555555,'老六',25,'18982379506','四川',55.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(666666,'老七',19,'18982379506','四川',25.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(777777,'老八',20,'18982379506','四川',61.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(888888,'九叔',21,'18982379506','四川',89.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(999999,'欧阳',21,'18982379506','四川',95.00);
insert into Student (s_number,s_name,s_age,s_tel,s_address,s_score) values(787878,'老张',23,'18982379506','四川',82.00);

select * from Student where s_score > 80 or s_score < 60

select * from Student where s_age > 18 order by s_score asc,s_number desc

select * from Student where s_age between 25 and 35

update Student set s_address = '成都',s_tel = '135***' where s_age > 18
