1、在数据库中新建一张 product表：包括字段p_id(int)、p_Name(varchar)、price(double)、num(int)、company(varchar)、address(varchar)

2、新增几条产品记录:电视机、冰箱、服装、手机；(按照下面的数据插入)

3、将所有电视机的数量修改为1000台，将所有手机的价格修改为2500元；

4、删除手机价格介于1000元和2000元之间的记录；

5、查询出所有价格大于200元的服装或价格小于5000元的冰箱；

例如：
1	电视	1000.00	800	长虹	成都	
2	电视	1000.00	1000	海尔	成都	
3	冰箱	1800.00	300	长虹	北京	
4	冰箱	3500.00	500	海尔	重庆	
5	服装	280.00	1000	雅戈尔	成都	
6	服装	1200.00	200	雅戈尔	北京	
7	手机	2500.00	200	华为	深圳	
8	手机	200.00	1200			
9	手机	204.00	1500			
10	手机	202.00	1000			
